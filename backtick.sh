#!/usr/bin/env bash

for name in "$@"; do
    echo "\`\`\`" >"$name.tmp"
    cat "$name" >>"$name.tmp"
    echo "\`\`\`" >>"$name.tmp"
    mv "$name.tmp" "$name"
done
