#!/usr/bin/env python3

import yaml
import sys

text = open(sys.argv[1]).read()
yaml = yaml.load(text, Loader=yaml.Loader)

redirect = open("redirect.toml", 'w')
html = open("dl.html", 'w')

for k,v in yaml["redirects"].items():
    d = v['description']
    t = v['target']
    print(f'''"/{k}" = "{t}"''', file = redirect)

    print(f'''<dt><a href="/{k}"><code>/{k}</code></a></dt>''', file=html)
    print(f'''<dd><a href="{t}"><code>{t}</code> ({d})</a></dt>''', file=html)

