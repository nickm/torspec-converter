# Tor specifications

These documents describe how Tor works, and try to do so in enough
detail to allow other compatible implementations of the Tor protocols.

They were once a separate set of text files, but in late 2023 we
migrated to use [`mdbook`](https://rust-lang.github.io/mdBook/).
We're in the process of updating these documents to improve their quality.

In addition to these specifications, you should look at the set of <a
href="./proposals/BY_STATUS.html#finished-proposals-implemented-specs-not-merged">`FINISHED`
Tor proposals</a>: They are the ones that have been implemented, but
not yet merged into these documents.

<a href="./proposals/index.html">You can also see all the Tor
proposals here.</a>

----

# Permalinks

Additionally, these URLs at `spec.toprorject.org` are intended to be
long-term permalinks.

TODO: Revise them to point somewhere sensible again.

<dl>
<dt><a href="/address-spec"><code>/address-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/address-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/address-spec.txt</code> (Special Hostnames in Tor)</a></dt>
<dt><a href="/bandwidth-file-spec"><code>/bandwidth-file-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/bandwidth-file-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/bandwidth-file-spec.txt</code> (Directory Authority Bandwidth File spec)</a></dt>
<dt><a href="/bridgedb-spec"><code>/bridgedb-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/bridgedb-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/bridgedb-spec.txt</code> (BridgeDB specification)</a></dt>
<dt><a href="/cert-spec"><code>/cert-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/cert-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/cert-spec.txt</code> (Ed25519 certificates in Tor)</a></dt>
<dt><a href="/collector-protocol"><code>/collector-protocol</code></a></dt>
<dd><a href="https://gitweb.torproject.org/collector.git/tree/src/main/resources/docs/PROTOCOL"><code>https://gitweb.torproject.org/collector.git/tree/src/main/resources/docs/PROTOCOL</code> (Protocol of CollecTor's File Structure)</a></dt>
<dt><a href="/control-spec"><code>/control-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/control-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/control-spec.txt</code> (Tor control protocol, version 1)</a></dt>
<dt><a href="/dir-spec"><code>/dir-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/dir-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/dir-spec.txt</code> (Tor directory protocol, version 3)</a></dt>
<dt><a href="/dir-list-spec"><code>/dir-list-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/dir-list-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/dir-list-spec.txt</code> (Tor Directory List file format)</a></dt>
<dt><a href="/ext-orport-spec"><code>/ext-orport-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/ext-orport-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/ext-orport-spec.txt</code> (Extended ORPort for pluggable transports)</a></dt>
<dt><a href="/gettor-spec"><code>/gettor-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/gettor-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/gettor-spec.txt</code> (GetTor specification)</a></dt>
<dt><a href="/padding-spec"><code>/padding-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/padding-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/padding-spec.txt</code> (Tor Padding Specification)</a></dt>
<dt><a href="/path-spec"><code>/path-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/path-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/path-spec.txt</code> (Tor Path Specification)</a></dt>
<dt><a href="/pt-spec"><code>/pt-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/pt-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/pt-spec.txt</code> (Tor Pluggable Transport Specification, version 1)</a></dt>
<dt><a href="/rend-spec"><code>/rend-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/rend-spec-v3.txt"><code>https://gitweb.torproject.org/torspec.git/tree/rend-spec-v3.txt</code> (Tor Onion Service Rendezvous Specification, Version 2)</a></dt>
<dt><a href="/rend-spec-v2"><code>/rend-spec-v2</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/attic/rend-spec-v2.txt"><code>https://gitweb.torproject.org/torspec.git/tree/attic/rend-spec-v2.txt</code> (Tor Onion Service Rendezvous Specification, Version 2)</a></dt>
<dt><a href="/rend-spec-v3"><code>/rend-spec-v3</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/rend-spec-v3.txt"><code>https://gitweb.torproject.org/torspec.git/tree/rend-spec-v3.txt</code> (Tor Onion Service Rendezvous Specification, Version 3)</a></dt>
<dt><a href="/socks-extensions"><code>/socks-extensions</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/socks-extensions.txt"><code>https://gitweb.torproject.org/torspec.git/tree/socks-extensions.txt</code> (Tor's extensions to the SOCKS protocol)</a></dt>
<dt><a href="/srv-spec"><code>/srv-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/srv-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/srv-spec.txt</code> (Tor Shared Random Subsystem Specification)</a></dt>
<dt><a href="/tor-fw-helper-spec"><code>/tor-fw-helper-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/attic/tor-fw-helper-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/attic/tor-fw-helper-spec.txt</code> (Tor's (little) Firewall Helper specification)</a></dt>
<dt><a href="/tor-spec"><code>/tor-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/tor-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/tor-spec.txt</code> (Tor Protocol Specification)</a></dt>
<dt><a href="/torbrowser-design"><code>/torbrowser-design</code></a></dt>
<dd><a href="https://2019.www.torproject.org/projects/torbrowser/design/"><code>https://2019.www.torproject.org/projects/torbrowser/design/</code> (The Design and Implementation of the Tor Browser)</a></dt>
<dt><a href="/version-spec"><code>/version-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/version-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/version-spec.txt</code> (How Tor Version Numbers Work)</a></dt>
<dt><a href="/tor-design"><code>/tor-design</code></a></dt>
<dd><a href="https://svn.torproject.org/svn/projects/design-paper/tor-design.pdf"><code>https://svn.torproject.org/svn/projects/design-paper/tor-design.pdf</code> (Tor: The Second-Generation Onion Router)</a></dt>
<dt><a href="/walking-onions"><code>/walking-onions</code></a></dt>
<dd><a href="https://github.com/nmathewson/walking-onions-wip/tree/master/specs"><code>https://github.com/nmathewson/walking-onions-wip/tree/master/specs</code> (Walking Onions specifications (work in progress))</a></dt>

</dl>
