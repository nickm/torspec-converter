#!/usr/bin/env python3
#
# Goal: Read tor specs, output mdbook.


import re
import os
import itertools

HEADING_PATTERN = re.compile(
   r'''
    ^
     (Appendix\s+)?
     (
         (?:[A-Z0-9]+\.)+
         (?:[A-Z0-9]+)?
     )
     :?\s+(.*)''',
    re.MULTILINE|re.VERBOSE
)

class FileCfg:
    def __init__(self, basename, base_indent=None, split_at_level=None, title=None, add_depth=0, split_sections=()):
        self.basename = basename
        self.base_indent = base_indent
        self.split_at_level = split_at_level
        self.title = title
        self.add_depth = add_depth
        self.split_sections = list(split_sections)

    def parse(self, torspec_dir):
        filename = os.path.join(torspec_dir, self.basename)
        lines = open(filename).readlines()

        if self.base_indent == None:
            self.base_indent = mode(indent_of_line(line) for line in lines if not line.isspace())

        if self.split_at_level == None:
            if len(lines) < 600:
                self.split_at_level = 0
            else:
                self.split_at_level = 1

        if self.title is None:
            self.title = next(line.strip() for line in lines if not line.isspace())

        spec = Spec(self)
        for graf in paragraphs(lines):
            graf = self.classify_paragraph(graf)
            spec.push_graf(graf)
        spec.note_splits()
        return spec

    def classify_paragraph(self, para):
        if len(para) == 1 and (m := HEADING_PATTERN.match(para[0])):
            orig_number = m.group(2)
            orig_title = m.group(3)
            if m.group(1):
                n = orig_number.rstrip(".:")
                title = f"Appendix {n}: {orig_title}"
                fname = title_to_fname(orig_title)
            else:
                title = orig_title
                fname = title_to_fname(title)

            return Heading(orig_number, title, fname)
        elif len(para) > 1:
            ind = uniform_indent(para)
            if ind == None or ind > self.base_indent:
                return Verbatim(para)
            else:
                return Body(para)
        else:
            assert len(para) == 1
            return Ambiguous(para)

class Spec:
    def __init__(self, cfg):
        self.cfg = cfg
        self.grafs = []

    def push_graf(self, graf):
        if len(self.grafs) == 0:
            self.grafs.append(graf)
        elif self.grafs[-1].merge_from(graf):
            return
        else:
            self.grafs.append(graf)

    def note_splits(self):
        # for every section in cfg.split_sections see if
        still_to_split = set(self.cfg.split_sections)
        splitting = False
        for g in self.grafs:
            if g.kind() == 'H':
                if g.fname in still_to_split:
                    still_to_split.remove(g.fname)
                    depth = g.depth
                    splitting = True
                    g.own_file = True
                elif splitting and g.depth <= depth:
                    splitting = False
                elif splitting and g.depth == depth + 1:
                    g.own_file = True

        if still_to_split:
            print(f"DID NOT FIND {still_to_split}")

    def dump_md(self, target_dir, summary_file, redirect_file):
        bname = os.path.splitext(self.cfg.basename)[0]

        if self.cfg.split_at_level == 0:
            outfname_base = bname
            outfname = outfname_base + ".md"
            i = ind(self.cfg.add_depth)
            print(f"{i}- [`{self.cfg.title}`](./{outfname})",
                  file=summary_file)
        else:
            outfname_base = bname + "-intro"
            outfname = outfname_base + ".md"
            i = ind(self.cfg.add_depth)
            print(f"{i}- [`{self.cfg.title}`](./{outfname})", file=summary_file)
            os.makedirs(os.path.join(target_dir, bname))


        print(f'Redirect 302 "/{bname}" "/{outfname_base}.html"',
              file=redirect_file)

        outfname = os.path.join(target_dir, outfname)

        out = open(outfname, 'w')
        try:
            for graf in self.grafs:
                if split := graf.split_info(self.cfg):
                    out.close()
                    outfname, depth, title = split
                    outfname = os.path.join(bname, outfname)
                    i = ind(depth+self.cfg.add_depth)
                    print(f"{i}- [{title}](./{outfname})", file=summary_file)
                    out = open(os.path.join(target_dir, outfname), 'w')

                graf.dump_md(out, self.cfg)
                out.write("\n")

        finally:
            out.close()

def paragraphs(f):
    lines = []
    for line in f:
        if line.strip() == "":
            if lines:
                yield lines
                lines = []
        else:
             lines.append(line)
    if lines:
        yield lines


def indent_of_line(line):
    return len(line) - len(line.lstrip(" "))

def uniform_indent(para):
    s = set(indent_of_line(line) for line in para)
    if len(s) == 1:
        return list(s)[0]
    else:
        return None

def ind(depth):
    return "  " * depth

def mode(lst):
    counts = {}
    for item in lst:
        try:
            counts[item] += 1
        except KeyError:
            counts[item] = 1
    counts = sorted((v,k) for k,v in counts.items())
    return counts[-1][1]

class Item:
    def __init__(self):
        pass

    def get_text(self):
        return self.body

    def kind(self):
        raise NotImplemented()

    def merge_from(self, next_item):
        return False

    def split_info(self, cfg):
        return None

class Heading(Item):
    def __init__(self, number, title, fname):
        Item.__init__(self)
        self.number = number.rstrip(".")
        self.depth = self.number.count(".") + 1
        self.title = title
        self.fname = fname
        self.own_file = False

    def dump_md(self, out, cfg):
        anchor = f"{cfg.basename }-{self.number}"
        print(f'<a id="{anchor}"></a>', file=out)
        hdr = "#" * self.depth
        print(f"{hdr} {self.title}", file=out)

    def split_info(self, cfg):
        if self.depth <= cfg.split_at_level or self.own_file:
            return (self.fname + ".md", self.depth, self.title)
        else:
            return None

    def kind(self):
        return "H"

FILLERS = set("and or in tor the a an".split())
def title_to_fname(title):
    title = re.sub(r'[^\-\w\s]+', '', title.lower())
    words = list(w for w in title.split() if w not in FILLERS)
    truncated = []
    total = 0
    for w in words:
        if total > 40:
            break
        truncated.append(w)
        total += len(w)
    return "-".join(truncated)


class Body(Item):
    def __init__(self, body):
        Item.__init__(self)
        self.body = body

    def dump_md(self, out, _cfg):
        for line in self.body:
            out.write(line.lstrip(" "))

    def kind(self):
        return "B"


class Verbatim(Item):
    def __init__(self, body):
        Item.__init__(self)
        self.body = body

    def kind(self):
        return "V"

    def dump_md(self, out, _cfg):
        out.write("```text\n")
        for line in self.body:
            out.write(line)
        out.write("```\n")

    def merge_from(self, next_item):
        if next_item.kind() in "VA":
            self.body.append("\n")
            self.body.extend(next_item.body)
            return True
        else:
            return False

class Ambiguous(Item):
    def __init__(self, body):
        Item.__init__(self)
        self.body = body

    def dump_md(self, out, _cfg):
        for line in self.body:
            out.write(line.lstrip())

    def kind(self):
        return "A"

class Section:
    def __init__(self, title):
        self.title = title
    def parse(self, _dir):
        return self

    def dump_md(self, target_dir, summary_file, redirect_file):
        print(f"# {self.title}\n", file=summary_file)

class Divider:
    def __init__(self):
        pass
    def parse(self, _dir):
        return self

    def dump_md(self, target_dir, summary_file, redirect_file):
        print("-------", file=summary_file)

class MdFile:
    def __init__(self, md_filename, title, add_depth=0, marker="- "):
        self.md_filename = md_filename
        self.title = title
        self.add_depth = add_depth
        self.marker = marker

    def parse(self, _dir):
        return self

    def dump_md(self, target_dir, summary_file, redirect_file):
        i = ind(self.add_depth)
        print(f"{i}{self.marker}[{self.title}](./{self.md_filename})", file=summary_file)

FILES = [
    MdFile("INTRO.md", title="Introduction", marker=""),
    
    Section("The core Tor protocol"),
    FileCfg("tor-spec.txt",
            split_sections=[
                "circuit-management",
                "application-connections-stream-management",
            ]),

    FileCfg("cert-spec.txt", add_depth=1),
    FileCfg("dir-spec.txt",
            split_sections=[
                "directory-authority-operation-formats",
                "router-operation-formats",
                "uploading-server-descriptors-extra-info-documents",
                "exchanging-votes"
            ]),
    FileCfg("srv-spec.txt", add_depth=1),
    FileCfg("path-spec.txt", base_indent=4,
            split_sections=["building-circuits"]),
    FileCfg("guard-spec.txt"),
    FileCfg("padding-spec.txt"),
    MdFile("dos-spec.md", title="Preventing Denial-Of-Service"),

    Section("Additional behaviors for clients"),
    FileCfg("socks-extensions.txt"),
    FileCfg("address-spec.txt"),

    Section("Onion services"),
    FileCfg("rend-spec-v3.txt",
            split_sections=[
                "generating-publishing-hidden-service-descriptors"
            ]),

    Section("Anticensorship tools and protocols"),
    FileCfg("bridgedb-spec.txt"),
    FileCfg("ext-orport-spec.txt"),
    FileCfg("pt-spec.txt",
            split_sections=["specification"]),
    FileCfg("gettor-spec.txt"),

    Section("For C Tor only"),
    FileCfg("control-spec.txt",
            title="The Tor Control Protocol",
            split_sections=[
                # "commands",
                # "replies",
                # "asynchronous-events",
            ]),
    FileCfg("version-spec.txt",
            title="How Tor Version Numbers Work"),

    Section("Less commonly needed file formats"),
    FileCfg("bandwidth-file-spec.txt",
            split_sections=["format-details"]),

    Section("Implementation details"),
    FileCfg("dir-list-spec.txt"),

    Section("Reserved names and numbers"),
    FileCfg("param-spec.txt"),

    Section("Unfinished"),
    FileCfg("glossary.txt"),
]

# ------------------------------

import sys
import shutil


def run(torspec_dir, output_dir):
    src_dir = os.path.join(output_dir, "spec")

    os.makedirs(src_dir)
    os.makedirs(os.path.join(output_dir, "book"), exist_ok=True)

    summary_file = open(os.path.join(src_dir, "SUMMARY.md"), 'w')
    summary_file.write("# Summary\n\n")

    redirect_file = open(os.path.join(src_dir, ".htaccess"), 'w')
    print("# Automatically generated redirects", file=redirect_file)
    print("# ")

    for fc in FILES:
        spec = fc.parse(torspec_dir)
        spec.dump_md(src_dir, summary_file, redirect_file)

    summary_file.close()
    redirect_file.close()

if __name__ == '__main__':
    run(sys.argv[1], sys.argv[2])

