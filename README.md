# Tools to convert torspec.git from text files to mdbook

Right now this assumes that you have a clean checkout of torspec.git
in `~/src/torspec-conversion`.  It requires that you have mdbook
installed.  It makes a new `spec-conversion` branch in your torspec.git
repository, deleting it if it already existed.

The script to run is "./specify.sh".

Since this tool is only intended to convert our repository once, it
is not terribly bulletproof.  After we have actually converted the
repository, we won't run this tool again.

For more information on the conversion, see proposal `xxx-using-mdbook.md`
