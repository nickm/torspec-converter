#!/usr/bin/env bash

COMMIT() {
    echo "COMMIT: $1"
    git commit -a -m "$1"
}

set -e -u -o pipefail

SPEC_DIR="$HOME/src/torspec-conversion"
SCRIPT_DIR=$(realpath $(dirname "$0"))

if ! [ -d "$SPEC_DIR" ]; then
    echo "no $SPEC_DIR"
    exit 1
fi

if ! [ -x "$SCRIPT_DIR/grinder.py" ]; then
    echo "$SCRIPT_DIR contents are wrong."
    exit 1
fi

if ! which mdbook 2>/dev/null ; then
    echo "mdbook not installed"
    exit 1
fi

cd "$SPEC_DIR"

if [ -z "$(git status --porcelain)" ]; then
    echo "working directory is clean"
else
    echo "found uncommitted changes. Not proceding!"
    exit 1
fi

git checkout main
git checkout -B spec_conversion

cat >>.gitignore <<EOF

# mdbook outputs
/html
EOF
COMMIT "Add mdbook default output to .gitignore"

TXT_SPEC_DIR="$(pwd)/attic/text_formats"
mkdir -p "$TXT_SPEC_DIR"
cat >"$TXT_SPEC_DIR/README.md" <<EOF
This directory contains our specifications before our conversion
to mdbook on $(date).
EOF

git mv *.txt "$TXT_SPEC_DIR"
git add "$TXT_SPEC_DIR/README.md"
COMMIT 'Move all text-only specifications into the OLD_TXT directory.'

"$SCRIPT_DIR"/grinder.py "$TXT_SPEC_DIR" .
git add spec/
COMMIT 'Convert text specifications to mdbook.'

git mv *.md spec/
COMMIT 'Move all MD-only specifications into spec/.'

cp -r "$SCRIPT_DIR"/verbatim/* "$SPEC_DIR"
git add .
COMMIT 'Copy all verbatim items into spec directory.'

"$SCRIPT_DIR"/backtick.sh "$SPEC_DIR"/proposals/*.txt
git add proposals
COMMIT 'Wrap text proposals in backticks.'

pushd ./mdbook/spec && mdbook build
mkdir -p ./theme
git add ./theme
COMMIT 'Commit default mdbook theme (specs)'
popd

pushd ./mdbook/proposals && mdbook build
mkdir -p ./theme
git add ./theme
COMMIT 'Commit default mdbook theme (proposals)'
popd


